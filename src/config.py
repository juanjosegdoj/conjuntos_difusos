KIND_OF_GRAPHIC = ['Discrete', 'Continuous']

# The main label shown in the window
MAIN_LABEL = 'Fussy Operator'

POINT_LABEL = 'Point'

KIND_LABEL = 'Kind'

LOWER_LIMIT_LABEL = 'Lower Limit'
UPPER_LIMIT_LABEL = 'Upper Limit'

REMOTENESS = 'Remoteness'
REMOTENESS_LIST = ['so_near', 'near', 'far', 'so_far']

LOWER_LIMIT_VALUE = 50
UPPER_LIMIT_VALUE = 100
