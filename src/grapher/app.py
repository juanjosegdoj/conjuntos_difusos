from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from src.config import LOWER_LIMIT_VALUE, KIND_OF_GRAPHIC, REMOTENESS_LIST, UPPER_LIMIT_VALUE
from src.functions import create_subplot


class AppForm(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle('Fussy Operator')

        self.create_menu()
        self.create_main_frame()
        self.create_status_bar()

        self.pointBox.setText('60')
        self.on_draw()

    def save_plot(self):
        file_choices = "PNG (*.png)|*.png"

        path, ext = QFileDialog.getSaveFileName(self,
                                                'Save file', '',
                                                file_choices)
        path = path.encode('utf-8')
        if not path[-4:] == file_choices[-4:].encode('utf-8'):
            path += file_choices[-4:].encode('utf-8')
        print(path)
        if path:
            self.canvas.print_figure(path.decode(), dpi=self.dpi)
            self.statusBar().showMessage('Saved to %s' % path, 2000)

    def on_about(self):
        msg = """ A demo of using PyQt with matplotlib:

         * Use the matplotlib navigation bar
         * Add values to the text box and press Enter (or click "Draw")
         * Show or hide the grid
         * Drag the slider to modify the width of the bars
         * Save the plot to a file using the File menu
         * Click on a bar to receive an informative message
        """
        QMessageBox.about(self, "About the demo", msg.strip())

    def on_draw(self):
        """ Redraws the figure
        """
        self.lower_box.setRange(-int(self.upper_box.text().encode('utf-8')), int(self.upper_box.text().encode('utf-8')))
        self.upper_box.setRange(int(self.lower_box.text().encode('utf-8')),
                                2 * int(self.upper_box.text().encode('utf-8')))

        self.axes.clear()
        self.axes.grid(self.grid_cb.isChecked())

        if int(self.pointBox.text().encode('utf-8')) not in range(int(self.lower_box.text().encode('utf-8')),
                                                                  int(self.upper_box.text().encode('utf-8'))):
            self.status_text.setText(f"ERROR: Unable to graph a point outside the given range "
                                     f"({int(self.lower_box.text().encode('utf-8'))},"
                                     f"{int(self.upper_box.text().encode('utf-8'))})")
            return

        if int(self.lower_box.text().encode('utf-8')) >= int(self.upper_box.text().encode('utf-8')):
            self.status_text.setText("ERROR: Unable to generate a graph. Check the range given")
            return

        self.status_text.clear()

        flags = {
            'kind': self.kind_box.currentText(),
            'point': int(self.pointBox.text().encode('utf-8')),
            'proximity': self.proximity_box.currentText()
        }

        params = create_subplot(**flags)

        self.axes.plot(params['xs'], params['ys'])

        self.canvas.draw()

    def create_main_frame(self):
        self.main_frame = QWidget()

        self.dpi = 100
        self.fig = Figure((6.0, 5.0), dpi=self.dpi)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)

        self.axes = self.fig.add_subplot(111)

        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)

        # Other GUI controls

        point_label = QLabel('Point')
        self.pointBox = QLineEdit()
        self.pointBox.setMinimumWidth(200)
        self.pointBox.editingFinished.connect(self.on_draw)

        lower_label = QLabel('Lower Limit')
        self.lower_box = QSpinBox()
        self.lower_box.setValue(LOWER_LIMIT_VALUE)
        self.lower_box.valueChanged.connect(self.on_draw)

        upper_label = QLabel('Upper Limit')
        self.upper_box = QSpinBox()
        self.upper_box.setValue(UPPER_LIMIT_VALUE)
        self.upper_box.valueChanged.connect(self.on_draw)

        kind_label = QLabel('Kind of graphic')
        self.kind_box = QComboBox()
        self.kind_box.addItems(KIND_OF_GRAPHIC)
        self.kind_box.currentIndexChanged.connect(self.on_draw)

        proximity_label = QLabel('Proximity')
        self.proximity_box = QComboBox()
        self.proximity_box.addItems(REMOTENESS_LIST)
        self.proximity_box.currentIndexChanged.connect(self.on_draw)

        self.draw_button = QPushButton("&Draw")
        self.draw_button.clicked.connect(self.on_draw)

        self.grid_cb = QCheckBox("Show &Grid")
        self.grid_cb.setChecked(False)
        self.grid_cb.stateChanged.connect(self.on_draw)

        hbox = QHBoxLayout()

        for w in [point_label, self.pointBox, lower_label, self.lower_box, upper_label, self.upper_box, kind_label,
                  self.kind_box, proximity_label, self.proximity_box, self.draw_button]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignLeft)

        vbox = QVBoxLayout()
        vbox.addWidget(self.canvas)
        vbox.addWidget(self.mpl_toolbar)
        vbox.addLayout(hbox)

        self.main_frame.setLayout(vbox)
        self.setCentralWidget(self.main_frame)

    def create_status_bar(self):
        self.status_text = QLabel("This is Fussy Operator")
        self.statusBar().addWidget(self.status_text, 1)

    def create_menu(self):
        self.file_menu = self.menuBar().addMenu("&File")

        load_file_action = self.create_action("&Save plot",
                                              shortcut="Ctrl+S", slot=self.save_plot,
                                              tip="Save the plot")
        quit_action = self.create_action("&Quit", slot=self.close,
                                         shortcut="Ctrl+Q", tip="Close the application")

        self.add_actions(self.file_menu,
                         (load_file_action, None, quit_action))

        self.help_menu = self.menuBar().addMenu("&Help")
        about_action = self.create_action("&About",
                                          shortcut='F1', slot=self.on_about,
                                          tip='About the demo')

        self.add_actions(self.help_menu, (about_action,))

    def add_actions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def create_action(self, text, slot=None, shortcut=None,
                      icon=None, tip=None, checkable=False):
        action = QAction(text, self)
        if icon is not None:
            action.setIcon(QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            action.triggered.connect(slot)
        if checkable:
            action.setCheckable(True)
        return action
