import sys

from PyQt5.QtWidgets import QApplication

from src.grapher.app import AppForm

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = AppForm()
    form.show()
    app.exec_()
