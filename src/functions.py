import matplotlib.pyplot as plt
import numpy as np
from sympy import latex, sympify

functions = {
    "Discrete": {
        "so_near": "1",
        "near": "2",
        "far": "5",
        "so_far": "10"
    },
    "Continuous": {
        "so_near": '1 / (1 + ({point} - x)**4)',
        "near": '1 / (1 + ({point} - x)**2)',
        "far": '1 / (1 + ({point} - x)**4/6)',
        "so_far": '1 / (1 + ({point} - x)**2/6)'
    }
}


def get_function(kind, point, proximity):
    """
    Purpose: Allow Easy access to function and latex expression of one function

    :param point: point where function will be centered.
    :param kind: Discrete or Continuous variable
    :param proximity: so_near, near, far or so_far
    :return: a lambda function and latex expression of function selected
    """

    str_function = functions[kind][proximity].format(point=point)
    function, latex_exp = lambda x: eval(str_function), latex(sympify(str_function))

    return function, latex_exp


def create_subplot(kind, point, proximity):
    if kind == "Continuous":

        function, latex_exp = get_function(kind, point, proximity)
        x_limit = (-9 + point, 9 + point)
        y_limit = (-1, 1.5)
        title = "A := {" + f'x E R | x is {proximity} from {str(point)}' + "}"
        interval = (-11 + point, 14 + point, 0.001)
        x = np.arange(*interval)
        y = function(x)
        style = "-"

    elif kind == "Discrete":

        latex_exp = f"x = {point}"
        lower_limit = point - 20
        upper_limit = point + 20

        x_limit = (lower_limit - 1, upper_limit + 1)
        y_limit = (-1, 1.5)
        title = "A := {" + f'x E R | x is {proximity} from {str(point)}' + "}"

        hyper_parameter = functions["Discrete"][proximity]
        x = np.array([lower_limit, point - int(hyper_parameter), point, point + int(hyper_parameter), upper_limit])
        y = np.array([0, 0, 1, 0, 0])
        style = 'go--'

    else:
        raise Exception(f"Kind variable '{kind}' not defined.")

    # Plotting function fig
    fig, ax = plt.subplots()

    params = {
        'xs': x,
        'ys': y,
        'xlabel': 'Label X',
        'ylabel': 'Label Y',
        'xlim': x_limit,
        'ylim': y_limit
    }

    ax.plot(x, y, style, label=f"${latex_exp}$  - {proximity}")
    ax.set_xlabel('Label X')
    ax.set_ylabel('Label Y')
    ax.set_title(title)
    ax.set_xlim(*x_limit)
    ax.set_ylim(*y_limit)
    ax.grid()
    ax.legend()

    fig.show()
    del fig, ax
    return params
