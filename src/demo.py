from src.functions import create_subplot
"""
# Input data
kind_variable = "Continuous"
proximity = "near"
point = 20
"""

# Input data
kind_variable = "Discrete"
proximity = "so_far"
point = 5

fig, ax = create_subplot(kind_variable, point, proximity)
fig.show()
